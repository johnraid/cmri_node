

#include <Wire.h>  // Wire.h library is required to use SX1509 lib
#include <SparkFunSX1509.h>  // Include the SX1509 library

//right bottom
#define HEAD1_RED		0
#define HEAD1_GREEN		2
#define HEAD1_YELLOW	3

//right top
#define HEAD2_RED		4
#define HEAD2_GREEN		6
#define HEAD2_YELLOW	7

//left bottom
#define HEAD3_RED		15
#define HEAD3_GREEN		13
#define HEAD3_YELLOW	12

// left top
#define HEAD4_RED		11
#define HEAD4_GREEN		9
#define HEAD4_YELLOW	8


// Uncomment one of the four lines to match your SX1509's address
//  pin selects. SX1509 breakout defaults to [0:0] (0x3E).
const byte SX1509_ADDRESS1 = 0x3E;  // SX1509 I2C address (00)
const byte SX1509_ADDRESS2 = 0x3F;  // SX1509 I2C address (01)
//const byte SX1509_ADDRESS3 = 0x70;  // SX1509 I2C address (10)
//const byte SX1509_ADDRESS4 = 0x71;  // SX1509 I2C address (11)

// Arduino pin definitions
const byte resetPin = 8;
sx1509Class signal1(SX1509_ADDRESS1);
sx1509Class signal2(SX1509_ADDRESS2);

void init_i2c_heads(){
	signal1.init();
	
	/*
	signal1.pinDir(HEAD1_RED, OUTPUT); 
	signal1.pinDir(HEAD1_GREEN, OUTPUT); 
	signal1.pinDir(HEAD1_YELLOW, OUTPUT); 
	signal1.pinDir(HEAD2_RED, OUTPUT); 
	signal1.pinDir(HEAD2_GREEN, OUTPUT); 
	signal1.pinDir(HEAD2_YELLOW, OUTPUT); 
	*/
	signal1.pinDir(0, OUTPUT); 
	signal1.pinDir(1, OUTPUT); 
	signal1.pinDir(2, OUTPUT); 
	signal1.pinDir(3, OUTPUT); 
	signal1.pinDir(4, OUTPUT); 
	signal1.pinDir(5, OUTPUT); 
	signal1.pinDir(6, OUTPUT); 
	signal1.pinDir(7, OUTPUT); 
	signal1.pinDir(8, OUTPUT); 
	signal1.pinDir(9, OUTPUT); 
	signal1.pinDir(10, OUTPUT); 
	signal1.pinDir(11, OUTPUT); 
	signal1.pinDir(12, OUTPUT); 
	signal1.pinDir(13, OUTPUT); 
	signal1.pinDir(14, OUTPUT); 
	signal1.pinDir(15, OUTPUT); 
	
	signal2.pinDir(0, OUTPUT); 
	signal2.pinDir(1, OUTPUT); 
	signal2.pinDir(2, OUTPUT); 
	signal2.pinDir(3, OUTPUT); 
	signal2.pinDir(4, OUTPUT); 
	signal2.pinDir(5, OUTPUT); 
	signal2.pinDir(6, OUTPUT); 
	signal2.pinDir(7, OUTPUT); 
	signal2.pinDir(8, OUTPUT); 
	signal2.pinDir(9, OUTPUT); 
	signal2.pinDir(10, OUTPUT); 
	signal2.pinDir(11, OUTPUT); 
	signal2.pinDir(12, OUTPUT); 
	signal2.pinDir(13, OUTPUT); 
	signal2.pinDir(14, OUTPUT); 
	signal2.pinDir(15, OUTPUT); 
	
	Set_Mast1(0xFF);
	Set_Mast2(0xFF);
	Set_Mast3(0xFF);
	Set_Mast4(0xFF);
	
}

void Set_Mast1(int lights){
	signal1.writePin(0, !(lights & 0x01));  
	signal1.writePin(1, !(lights & 0x02));  
	signal1.writePin(2, !(lights & 0x04));  
	signal1.writePin(3, !(lights & 0x08));  
	signal1.writePin(4, !(lights & 0x10));  
	signal1.writePin(5, !(lights & 0x20));  
	signal1.writePin(6, !(lights & 0x40));  
	signal1.writePin(7, !(lights & 0x80));  
}

void Set_Mast2(int lights){
	signal1.writePin(8, !(lights & 0x01)); 
	signal1.writePin(9, !(lights & 0x02)); 
	signal1.writePin(10, !(lights & 0x04));
	signal1.writePin(11, !(lights & 0x08));
	signal1.writePin(12, !(lights & 0x10));
	signal1.writePin(13, !(lights & 0x20));
	signal1.writePin(14, !(lights & 0x40));
	signal1.writePin(15, !(lights & 0x80));
}

void Set_Mast3(int lights){
	signal2.writePin(0, !(lights & 0x01));  
	signal2.writePin(1, !(lights & 0x02));  
	signal2.writePin(2, !(lights & 0x04));  
	signal2.writePin(3, !(lights & 0x08));  
	signal2.writePin(4, !(lights & 0x10));  
	signal2.writePin(5, !(lights & 0x20));  
	signal2.writePin(6, !(lights & 0x40));  
	signal2.writePin(7, !(lights & 0x80));  
}

void Set_Mast4(int lights){
	signal2.writePin(8, !(lights & 0x01)); 
	signal2.writePin(9, !(lights & 0x02)); 
	signal2.writePin(10, !(lights & 0x04));
	signal2.writePin(11, !(lights & 0x08));
	signal2.writePin(12, !(lights & 0x10));
	signal2.writePin(13, !(lights & 0x20));
	signal2.writePin(14, !(lights & 0x40));
	signal2.writePin(15, !(lights & 0x80));
}

/*
void Set_Head(int lights){
	signal1.writePin(HEAD3_RED, !(lights & 0x01));  // Write pin LOW
	signal1.writePin(HEAD3_GREEN, !(lights & 0x02));  // Write pin HIGH
	signal1.writePin(HEAD3_YELLOW, !(lights & 0x04));  // Write pin HIGH
	signal1.writePin(HEAD4_RED, !(lights & 0x08));  // Write pin HIGH
	signal1.writePin(HEAD4_GREEN, !(lights & 0x10));  // Write pin HIGH
	signal1.writePin(HEAD4_YELLOW, !(lights & 0x20));  // Write pin HIGH
}
*/

/*
typedef enum{
	off = 0,
	green = 1,
	red = 2,
	yellow = 3
}color_t;

void set_head(int head_number, color_t color){
	switch (color){
        case off:
            cbi(PORTD, comm_red_pin);
            cbi(PORTD, comm_green_pin);
            break;
        case red:
            sbi(PORTD, comm_red_pin);
            cbi(PORTD, comm_green_pin);
            break;
        case green:
            cbi(PORTD, comm_red_pin);
            sbi(PORTD, comm_green_pin);
            break;
        case yellow:
            sbi(PORTD, comm_red_pin);
            sbi(PORTD, comm_green_pin);
            break;
        default:
            cbi(PORTD, comm_red_pin);
            cbi(PORTD, comm_green_pin);
            break;
    }
}

*/