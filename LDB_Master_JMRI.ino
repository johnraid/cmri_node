// C/MRI Teensy 3.1/3.2 Layout Data Concentrator
// Copyright 2021
// John Raid

#include <CMRI.h>
#include <Metro.h>
#include <EEPROM.h>

#define SW_VERSION		"0.7"
#define SW_VERSION_BIT  0x02	//This gets reported on CMRI as an input.  It will have to roll after 8
// version 0.3 - 1 IO expander and everything works
// version 0.4 - added the second IO expander
// version 0.6 - reports on C/MRI as 0x01.  Everything prior to this would report 0x00
// version 0.7 - added i2c signal head test
	
// Pick on for location.  I could do this without defines but there aren't that many
//#define GRAHAM
//#define MONMOUTH
//#define GLADSTONE
#define CONNET
//#define BRIDGE
//#define WOOD_TOWER
//#define BURLINGTON    There is a specific build for this because the Teensy at the depot has a bad Rx pin so they are mapped to Serial3
//#define WEST_YARD
//#define FIFTH_ST
//#define LUCAS
//#define DAYMAN
//#define MT_PLEASANT


// Pins
#define PIN_DET_B1_1	14
#define PIN_DET_B1_2	15
#define PIN_DET_B1_3	16

#define PIN_DET_B2_1	11
#define PIN_DET_B2_2	12
#define PIN_DET_B2_3	17

#define PIN_DET_B3_1	21
#define PIN_DET_B3_2	22
#define PIN_DET_B3_3	23

#define PIN_DET_PANEL1	24
#define PIN_DET_PANEL2	25
#define PIN_DET_PANEL3	26
#define PIN_DET_PANEL4	27
#define PIN_DET_PANEL5	28
#define PIN_DET_PANEL6	29
#define PIN_DET_PANEL7	30
#define PIN_DET_PANEL8	31
#define PIN_DET_PANEL9	32
#define PIN_DET_PANEL10	33

#define PIN_SIGNAL1_1	3
#define PIN_SIGNAL1_2	4
#define PIN_SIGNAL1_3	5
#define PIN_SIGNAL3_1	6
#define PIN_SIGNAL3_2	7
#define PIN_SIGNAL2_1	8
#define PIN_SIGNAL2_2	9
#define PIN_SIGNAL2_3	10


#ifdef GRAHAM
#define CMRI_NODE	10
boolean i2c_expansion = false;
#endif

#ifdef MONMOUTH
#define CMRI_NODE	11
boolean i2c_expansion = false;
#endif

#ifdef GLADSTONE
#define CMRI_NODE	12
boolean i2c_expansion = false;
#endif

#ifdef CONNET
#define CMRI_NODE	13
boolean i2c_expansion = true;
#endif

#ifdef BRIDGE
#define CMRI_NODE	20		// 14 I don't know why JMRI has issues with 14?  It was something else?
boolean i2c_expansion = false;
#endif

#ifdef WOOD_TOWER
#define CMRI_NODE	15
boolean i2c_expansion = true;
#endif

#ifdef BURLINGTON
#define CMRI_NODE	16
boolean i2c_expansion = false;
#endif

#ifdef WEST_YARD
#define CMRI_NODE	17
boolean i2c_expansion = false;
#endif

#ifdef FIFTH_ST
#define CMRI_NODE	18
boolean i2c_expansion = false;
#endif

#ifdef LUCAS
#define CMRI_NODE	19
boolean i2c_expansion = false;
#endif

#ifdef DAYMAN
#define CMRI_NODE	21
boolean i2c_expansion = false;
#endif

#ifdef MT_PLEASANT
#define CMRI_NODE	22
boolean i2c_expansion = true;
#endif

CMRI cmri(CMRI_NODE, 24, 48, Serial1);
Metro time20Hz = Metro(50);
Metro time1Hz = Metro(1000);

int panel_bits = 0;
int sensor_byte2 = 0;
int detector_bits = 0;

// these hang off the i2c bus as IO expanders
// mast 1 and 2 are on IO expander 1
int mast1 = 0xFF;
int mast2 = 0xFF;
// mast 3 and 4 are on IO expander 2
int mast3 = 0xFF;
int mast4 = 0xFF;
int discrete_outs = 0x00;

int test_heads = 0x00;

// We should make this a watchdog but for now C/MRI will have to assert control.
//int CMRI_in_control = false;
 
void setup() {
	Serial.begin(115200); 
	Serial1.begin(19200, SERIAL_8N2); // SERIAL_8N2 to match what JMRI expects CMRI hardware to use
	Serial1.setRX(0);
	Serial1.setTX(1);
	Serial1.transmitterEnable(2);
	
	// LED output for C/MRI to pulse
	pinMode(13, OUTPUT);
	
	// Detector 1 inputs
	pinMode(PIN_DET_B1_1, INPUT_PULLUP);	//nn017
	pinMode(PIN_DET_B1_2, INPUT_PULLUP);	//nn018
	pinMode(PIN_DET_B1_3, INPUT_PULLUP);	//nn019
	
	// Detector 2 inputs
	pinMode(PIN_DET_B3_1, INPUT_PULLUP);	//nn020
	pinMode(PIN_DET_B3_2, INPUT_PULLUP);	//nn021
	pinMode(PIN_DET_B3_3, INPUT_PULLUP);	//nn022
	
	// Detector 3 inputs
	pinMode(PIN_DET_B2_1, INPUT_PULLUP);	//nn023
	pinMode(PIN_DET_B2_2, INPUT_PULLUP);	//nn024
	pinMode(PIN_DET_B2_3, INPUT_PULLUP);	//nn025

	// Control panel inputs
	pinMode(PIN_DET_PANEL1, INPUT_PULLUP);	//nn001
	pinMode(PIN_DET_PANEL2, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL3, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL4, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL5, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL6, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL7, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL8, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL9, INPUT_PULLUP);
	pinMode(PIN_DET_PANEL10, INPUT_PULLUP);	//nn010
	
	// Outputs for signal heads or gates
	pinMode(PIN_SIGNAL1_1, OUTPUT);		//nn002
	pinMode(PIN_SIGNAL1_2, OUTPUT);
	pinMode(PIN_SIGNAL1_3, OUTPUT);
	pinMode(PIN_SIGNAL3_1, OUTPUT);
	pinMode(PIN_SIGNAL3_2, OUTPUT);
	pinMode(PIN_SIGNAL2_1, OUTPUT);
	pinMode(PIN_SIGNAL2_2, OUTPUT);
	pinMode(PIN_SIGNAL2_3, OUTPUT);	//nn009
	
	digitalWrite(PIN_SIGNAL1_3, LOW);	//Green
	digitalWrite(PIN_SIGNAL1_2, HIGH);	//Red
	digitalWrite(PIN_SIGNAL1_1, HIGH);	//Yellow
	
	digitalWrite(PIN_SIGNAL2_3, HIGH);	//Green
	digitalWrite(PIN_SIGNAL2_2, LOW);	//Red
	digitalWrite(PIN_SIGNAL2_1, HIGH);	//Yellow
	
	// This is the frequency the the IR detector filter is set to
	analogWriteFrequency(20, 24000);
	analogWriteFrequency(25, 24000);
	analogWriteFrequency(32, 24000);
	
	// This is for an IR emitter on pin 20 that is always on at 50%, 24kHz
	pinMode(20, OUTPUT);
	analogWrite(20, 128);
	
	cmri.set_address(CMRI_NODE);
	
	if(i2c_expansion){
		delay(1000);        //wait for 3.3V to power expansion boards
		init_i2c_heads();
	
		Set_Mast1(0x21);	// Red over red
		Set_Mast2(0x84);	// Red over red
		Set_Mast3(0x11);	// Red over red
		Set_Mast4(0x88);	// Red over red
	}
	
	print_version();
}

 
void loop() {
	cmri.process();
	
	if(Serial.available()){
		char c = Serial.read();
		switch(c){
			case 'r':
				Serial.print("Reset mode");
				cmri.debug_reset_mode();
				break;
			case 'v':
				print_version();
				break;
			case 't':
				Serial.print("Test Signal Heads");
				// Still need to figure out how best to do this
				test_heads = 0x80;
				break;
			default:
				Serial.print(c);
				break;
		}
	}
	
	if (time20Hz.check()){
		/*
		if(cmri.get_bit(0)){
			CMRI_in_control = true;
			digitalWrite(13, LOW);
		}else{
			CMRI_in_control = false;
		}
		*/
		
		// I could (should?) put this in an if(CMRI_in_control){} to force bit 13 set before the signals listen to the computer
		// we need to make these PWM and dim/brighten as the change for all signals
		discrete_outs = cmri.get_byte(0);
		digitalWrite(PIN_SIGNAL1_1, discrete_outs & 0x02);
		digitalWrite(PIN_SIGNAL1_2, discrete_outs & 0x04);
		digitalWrite(PIN_SIGNAL1_3, discrete_outs & 0x08);
		digitalWrite(PIN_SIGNAL3_1, discrete_outs & 0x10);
		digitalWrite(PIN_SIGNAL3_2, discrete_outs & 0x20);
		digitalWrite(PIN_SIGNAL2_1, discrete_outs & 0x40);
		digitalWrite(PIN_SIGNAL2_2, discrete_outs & 0x80);
		digitalWrite(PIN_SIGNAL2_3, cmri.get_bit(8));
	
		if(i2c_expansion){
			if(test_heads){
				mast1 = test_heads;
				mast2 = test_heads;
				mast3 = test_heads;
				mast4 = test_heads;
				
			}else{
				mast1 = cmri.get_byte(2);
				mast2 = cmri.get_byte(3);
				mast3 = cmri.get_byte(4);
				mast4 = cmri.get_byte(5);
			}
			//we could do a lot of checks here but if there is no command, turn red.
			//if ((heads & 0x07) == 0 || (heads & 0x38) == 0 ){
			//	heads = 0x09;
			//}
			//if(CMRI_in_control){
			Set_Mast1(mast1);
			Set_Mast2(mast2);
			Set_Mast3(mast3);
			Set_Mast4(mast4);
		}

		panel_bits = (digitalRead(PIN_DET_PANEL1) | 		// 24
					(digitalRead(PIN_DET_PANEL2) << 1) | 	// 25
					(digitalRead(PIN_DET_PANEL3) << 2) | 
					(digitalRead(PIN_DET_PANEL4) << 3) | 
					(digitalRead(PIN_DET_PANEL5) << 4) |
					(digitalRead(PIN_DET_PANEL6) << 5) | 
					(digitalRead(PIN_DET_PANEL7) << 6) | 
					(digitalRead(PIN_DET_PANEL8) << 7));	// 31
		
		// We need to "debounce" the detectors so they don't catch the space between cars			
		sensor_byte2 = (digitalRead(PIN_DET_PANEL9) | 		// 32
					(digitalRead(PIN_DET_PANEL10) << 1) |	// 33
					(SW_VERSION_BIT & 0x07) << 2 |
					(digitalRead(PIN_DET_B3_1) << 5) |
					(digitalRead(PIN_DET_B3_2) << 6) |
					(digitalRead(PIN_DET_B3_3) << 7));
		
		detector_bits = (digitalRead(PIN_DET_B1_1) | 
					(digitalRead(PIN_DET_B1_2) << 1) | 
					(digitalRead(PIN_DET_B1_3) << 2) |
					(digitalRead(PIN_DET_B2_1) << 3) |
					(digitalRead(PIN_DET_B2_2) << 4) |
					(digitalRead(PIN_DET_B2_3) << 5));
					
		cmri.set_byte(0, panel_bits);
		cmri.set_byte(1, sensor_byte2);
		cmri.set_byte(2, detector_bits);
		
		// This is just for the local signal until we get C/MRI up and fully running.
		/*
		if(!CMRI_in_control){
#ifdef BRIDGE
			// Signal 40 right 
			if(digitalRead(PIN_DET_PANEL2) == 0){
				digitalWrite(PIN_SIGNAL2_3, LOW);	//Green
				digitalWrite(PIN_SIGNAL2_2, HIGH);	//Red
				digitalWrite(PIN_SIGNAL2_1, HIGH);	//Yellow
			}else{
				digitalWrite(PIN_SIGNAL2_3, HIGH);	//Green
				digitalWrite(PIN_SIGNAL2_2, LOW);	//Red
				digitalWrite(PIN_SIGNAL2_1, HIGH);	//Yellow
			}
			
			// Signal 42 right 
			if(digitalRead(PIN_DET_PANEL1) == 0){
				digitalWrite(PIN_SIGNAL1_3, LOW);	//Green
				digitalWrite(PIN_SIGNAL1_2, HIGH);	//Red
				digitalWrite(PIN_SIGNAL1_1, HIGH);	//Yellow
			}else{
				digitalWrite(PIN_SIGNAL1_3, HIGH);	//Green
				digitalWrite(PIN_SIGNAL1_2, LOW);	//Red
				digitalWrite(PIN_SIGNAL1_1, HIGH);	//Yellow
			}
#endif		

#ifdef CONNET
			// 50R
			if(digitalRead(25) == 0){
				Set_Mast1(0x80);	// Green over dark
				Set_Mast3(0x11);	// Red over red
			// 50L
			}else if(digitalRead(24) == 0){
				Set_Mast1(0x21);	// Red over red
				Set_Mast3(0x04);	// Green over dark
			// 50S
			}else{
				Set_Mast1(0x21);	// Red over red
				Set_Mast3(0x11);	// Red over red
			}
			
			// 52L
			if(digitalRead(27) == 0){
				Set_Mast2(0x84);	// Red over red 1=yellow, 3=green, 4=blank, 8=redSet_Mast3(0x11);	// Red over red
				Set_Mast4(0x20);
			// 52R
			}else if(digitalRead(33) == 0){
				Set_Mast2(0x01);
				Set_Mast4(0x88);	// Red over red 1=yellow, 3=green, 4=blank, 8=red
			// 52S
			}else{
				Set_Mast2(0x84);	// Red over red 1=yellow, 3=green, 4=blank, 8=redSet_Mast3(0x11);	// Red over red
				Set_Mast4(0x88);	// Red over red 1=yellow, 3=green, 4=blank, 8=red
			}
#endif

#ifdef GLADSTONE
			// Signal S196.6 
			if(digitalRead(PIN_DET_B1_1) == 0){
				digitalWrite(PIN_SIGNAL2_3, LOW);	//Red 
				digitalWrite(PIN_SIGNAL2_2, HIGH);	//Yellow
				digitalWrite(PIN_SIGNAL2_1, HIGH);	//Green 
			}else{
				digitalWrite(PIN_SIGNAL2_3, HIGH);	//Red 
				digitalWrite(PIN_SIGNAL2_2, HIGH);	//Yellow
				digitalWrite(PIN_SIGNAL2_1, LOW);	//Green 
			}
			
			// Signal N196.6  
			if(digitalRead(PIN_DET_B1_3) == 0){
				digitalWrite(PIN_SIGNAL1_3, LOW);	//Red
				digitalWrite(PIN_SIGNAL1_2, HIGH);	//Yellow
				digitalWrite(PIN_SIGNAL1_1, HIGH);	//Green
			}else{
				digitalWrite(PIN_SIGNAL1_3, HIGH);	//Red
				digitalWrite(PIN_SIGNAL1_2, HIGH);	//Yellow
				digitalWrite(PIN_SIGNAL1_1, LOW);	//Green
			}
#endif		

#ifdef WOOD_TOWER
			Set_Mast1(0x21);	// Red over red
			Set_Mast2(0x84);	// Red over red 1=yellow, 3=green, 4=blank, 8=redSet_Mast3(0x11);
#endif

#ifdef BURLINGTON
			if(digitalRead(PIN_DET_B1_1) == 0){
				digitalWrite(PIN_SIGNAL2_3, LOW);	//Red 
				digitalWrite(PIN_SIGNAL2_2, HIGH);	//Yellow
				digitalWrite(PIN_SIGNAL2_1, HIGH);	//Green 
			}else{
				digitalWrite(PIN_SIGNAL2_3, HIGH);	//Red 
				digitalWrite(PIN_SIGNAL2_2, HIGH);	//Yellow
				digitalWrite(PIN_SIGNAL2_1, LOW);	//Green 
			}
#endif	

		}
		*/
	}

	if (time1Hz.check()){
		if(digitalRead(13)){
			digitalWrite(13, LOW);
		}else{
			digitalWrite(13, HIGH);
		}
		
			//Serial1.print("1");
		Serial.print(cmri.get_last_comm_address(), DEC);
		Serial.print(" Detectors:");
		Serial.print((sensor_byte2 >> 5) & 0x07, BIN);
		Serial.print(" ");
		Serial.print((detector_bits >> 3) & 0x07, BIN);  
		Serial.print(" ");
		Serial.print(detector_bits & 0x07, BIN);  
		Serial.print(" Heads:");
		Serial.print(discrete_outs, HEX);
		Serial.print(", ");
		Serial.print(mast1, HEX);
		Serial.print(" ");
		Serial.print(mast2, HEX);
		Serial.print(" ");
		Serial.print(mast3, HEX);
		Serial.print(" ");
		Serial.print(mast4, HEX);
		Serial.print(" Panel:");
		Serial.print(sensor_byte2 & 0x03, BIN);
		Serial.print(" ");
		Serial.println(panel_bits, BIN);
		
		if(test_heads){
			test_heads = test_heads >> 1;
		}
	}
}

void print_version(void){
	Serial.println("Arduino LDC C/MRI");
	Serial.print("SW Version: ");
	Serial.println(SW_VERSION);
	Serial.print("Node ID: ");
	Serial.println(CMRI_NODE);
}
